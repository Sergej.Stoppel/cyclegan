#################################################################
# A CycleGan version that is trained on the cezanne2photo dataset
# This version is based on the paper "Unpaired Image-to-Image 
# Translation using Cycle-Consistent Adversarial Networks"
# by Jun-Yan Zhu, Taesung Park, Phillip Isola, Alexei A. Efros
# The authors provided open source code for the network in Torch, 
# this version is based on Keras.
# Created by Sergej Stoppel, timestamp: 2019.10.20 17:35
#################################################################

###################
# Load dependencies
###################
from random import random
from numpy import load
from numpy import zeros
from numpy import ones
from numpy import asarray
from numpy.random import randint
from keras.optimizers import Adam
from keras.initializers import RandomNormal
from keras.models import Model
from keras.models import Input
from keras.layers import Conv2D
from keras.layers import Conv2DTranspose
from keras.layers import LeakyReLU
from keras.layers import Activation
from keras.layers import Concatenate
from instance_normalization import InstanceNormalization
from matplotlib import pyplot
 

################################
# Define the discriminator model
################################
def define_discriminator(image_shape):
	# Random weight initialization
	init = RandomNormal(stddev=0.02)

	# Source image input
	in_image = Input(shape=image_shape)

    ##########################
    # Define the discriminator
    # The Authors used LeakyReLu with a slope of 0.2 to avoid zero slope issues
    # 
    ##########################
	# C64
	d = Conv2D(64, (4,4), strides=(2,2), padding='same', kernel_initializer=init)(in_image)
	d = LeakyReLU(alpha=0.2)(d)

	# C128
    # Instance normalization is not used in the first layer
    # (TODO) Why not batch normalization?
	d = Conv2D(128, (4,4), strides=(2,2), padding='same', kernel_initializer=init)(d)
	d = InstanceNormalization(axis=-1)(d)
	d = LeakyReLU(alpha=0.2)(d)

	# C256
	d = Conv2D(256, (4,4), strides=(2,2), padding='same', kernel_initializer=init)(d)
	d = InstanceNormalization(axis=-1)(d)
	d = LeakyReLU(alpha=0.2)(d)

	# C512
	d = Conv2D(512, (4,4), strides=(2,2), padding='same', kernel_initializer=init)(d)
	d = InstanceNormalization(axis=-1)(d)
	d = LeakyReLU(alpha=0.2)(d)

	# Second last output layer
	d = Conv2D(512, (4,4), padding='same', kernel_initializer=init)(d)
	d = InstanceNormalization(axis=-1)(d)
	d = LeakyReLU(alpha=0.2)(d)

	# Patch output
	patch_out = Conv2D(1, (4,4), padding='same', kernel_initializer=init)(d)

	# Define model
	model = Model(in_image, patch_out)

	# compile model
    # (TODO) Test out if Nadam performs better than Adam
    # Important! Set loss_weights to 0.5 to slow down the weight update
	model.compile(loss='mse', optimizer=Adam(lr=0.0002, beta_1=0.5), loss_weights=[0.5])
	return model


################################################
# Generator a resnet block as described in 
# "Deep Residual Learning for Image Recognition"
################################################
def resnet_block(n_filters, input_layer):
	# Random weight ibitialization
	init = RandomNormal(stddev=0.02)

	# First convolutional layer
	g = Conv2D(n_filters, (3,3), padding='same', kernel_initializer=init)(input_layer)
	g = InstanceNormalization(axis=-1)(g)
	g = Activation('relu')(g)

	# Second convolutional layer
	g = Conv2D(n_filters, (3,3), padding='same', kernel_initializer=init)(g)
	g = InstanceNormalization(axis=-1)(g)

	# Concatenate merge channel-wise with input layer
	g = Concatenate()([g, input_layer])

	return g


#######################################
# Define the standalone generator model
#######################################
def define_generator(image_shape, n_resnet=9):

	# Weight initialization
	init = RandomNormal(stddev=0.02)

	# Image input
	in_image = Input(shape=image_shape)

	###################################################################
    # The generator is adopted from "Perceptual Losses for 
    # Real-Time Style Transfer and Super Resolution" by Johnson et al.
    # The architecture is using 6 residual blocks for 128x128 images 
    # and 9 blovks for 256x256 images or higher resolution. Here we set
    # the number of blocks to 9  by default.
    ###################################################################

    # c7s1-64
	g = Conv2D(64, (7,7), padding='same', kernel_initializer=init)(in_image)
	g = InstanceNormalization(axis=-1)(g)
	g = Activation('relu')(g)

	# d128
	g = Conv2D(128, (3,3), strides=(2,2), padding='same', kernel_initializer=init)(g)
	g = InstanceNormalization(axis=-1)(g)
	g = Activation('relu')(g)

	# d256
	g = Conv2D(256, (3,3), strides=(2,2), padding='same', kernel_initializer=init)(g)
	g = InstanceNormalization(axis=-1)(g)
	g = Activation('relu')(g)

	# R256
	for _ in range(n_resnet):
		g = resnet_block(256, g)

	# u128
	g = Conv2DTranspose(128, (3,3), strides=(2,2), padding='same', kernel_initializer=init)(g)
	g = InstanceNormalization(axis=-1)(g)
	g = Activation('relu')(g)
	
    # u64
	g = Conv2DTranspose(64, (3,3), strides=(2,2), padding='same', kernel_initializer=init)(g)
	g = InstanceNormalization(axis=-1)(g)
	g = Activation('relu')(g)

	# c7s1-3 
	g = Conv2D(3, (7,7), padding='same', kernel_initializer=init)(g)
	g = InstanceNormalization(axis=-1)(g)
	out_image = Activation('tanh')(g)

	# Define model
	model = Model(in_image, out_image)
	return model


##################################################################################
# Define a composite model for updating generators by adversarial and cycle loss
# While the discriminators are trained directly on the real and generated images
# the generators are trained with the standard adversarial lost plus identity loss
# and the cycle loss, divided into forward abd backward loss. The identity loss is
# based on "Unsupervised Cross-Domain Image Generation" by Taigman et al.
#
# N.B. This composite model only trains one generator, but requires the weghts of 
# the discriminator and the second generator, therefore it will will generate a 
# warning because these models are set as non trainable.
##################################################################################
def define_composite_model(g_model_1, d_model, g_model_2, image_shape):

	# Make the generator trainable
	g_model_1.trainable = True

	# Mark discriminator as not trainable
	d_model.trainable = False

	# Mark other generator model as not trainable
	g_model_2.trainable = False

	# Discriminator element
	input_gen = Input(shape=image_shape)
	gen1_out = g_model_1(input_gen)
	output_d = d_model(gen1_out)

	# Identity element
    # The identity loss ensures two things: 1st The generator should not change 
    # the image if one from the target domain is provided.
    # 2nd. The generator should preserve the colors of the input image.
	input_id = Input(shape=image_shape)
	output_id = g_model_1(input_id)

	# Forward cycle
	output_f = g_model_2(gen1_out)
	# Backward cycle
	gen2_out = g_model_2(input_id)
	output_b = g_model_1(gen2_out)

	# Define model graph
	model = Model([input_gen, input_id], [output_d, output_id, output_f, output_b])
	# Optimization setup

	opt = Adam(lr=0.0002, beta_1=0.5)

	# Compile model with L2 norm for adversarial loss and L1 for identity and the cycle losses
	model.compile(loss=['mse', 'mae', 'mae', 'mae'], loss_weights=[1, 5, 10, 10], optimizer=opt)
	return model


##################################
# Load and prepare training images
##################################
def load_real_samples(filename):
	# Load the dataset
	data = load(filename)

	# Unpack arrays
	X1, X2 = data['arr_0'], data['arr_1']

	# Scale from [0,255] to [-1,1]
    # (TODO) Maybe I should save the data in the correct format right away...
	X1 = (X1 / 127.5) - 1.0
	X2 = (X2 / 127.5) - 1.0
	return [X1, X2]


#############################################################
# Select a batch of random samples, returns images and target
#############################################################
def generate_real_samples(dataset, n_samples, patch_shape):

	# Choose random instances
	ix = randint(0, dataset.shape[0], n_samples)

	# Retrieve selected images
	X = dataset[ix]

	# Generate 'real' class labels (1)
	y = ones((n_samples, patch_shape, patch_shape, 1))
	return X, y


########################################################
# Generate a batch of images, returns images and targets
########################################################
def generate_fake_samples(g_model, dataset, patch_shape):

	# Generate fake instance
	X = g_model.predict(dataset)

	# Create 'fake' class labels (0)
	y = zeros((len(X), patch_shape, patch_shape, 1))
	return X, y


###################################
# Save the generator models to file
###################################
def save_models(step, g_model_AtoB, g_model_BtoA):

	# Save the first generator model
	filename1 = 'g_model_AtoB_%06d.h5' % (step+1)
	g_model_AtoB.save(filename1)

	# Save the second generator model
	filename2 = 'g_model_BtoA_%06d.h5' % (step+1)
	g_model_BtoA.save(filename2)
	print('>Saved: %s and %s' % (filename1, filename2))


###################################
# Generate samples and save on disc
###################################
def summarize_performance(step, g_model, trainX, name, n_samples=5):

	# Select a sample of input images
	X_in, _ = generate_real_samples(trainX, n_samples, 0)

	# Generate translated images
	X_out, _ = generate_fake_samples(g_model, X_in, 0)
	
    # Scale all pixels from [-1,1] to [0,1]
	X_in = (X_in + 1) / 2.0
	X_out = (X_out + 1) / 2.0
	
    # Plot real images
	for i in range(n_samples):
		pyplot.subplot(2, n_samples, 1 + i)
		pyplot.axis('off')
		pyplot.imshow(X_in[i])
	
    # Plot translated image
	for i in range(n_samples):
		pyplot.subplot(2, n_samples, 1 + n_samples + i)
		pyplot.axis('off')
		pyplot.imshow(X_out[i])

	# Save plot to file
	filename1 = '%s_generated_plot_%06d.png' % (name, (step+1))
	pyplot.savefig(filename1)
	pyplot.close()


###################################
# Update image pool for fake images
###################################
def update_image_pool(pool, images, max_size=50):
	selected = list()
	for image in images:
		if len(pool) < max_size:
			# Stock the pool
			pool.append(image)
			selected.append(image)
		elif random() < 0.5:
			# Use image, but don't add it to the pool
			selected.append(image)
		else:
			# Replace an existing image and use replaced image
			ix = randint(0, len(pool))
			selected.append(pool[ix])
			pool[ix] = image
	return asarray(selected)


#######################
# Train cyclegan models
#######################
def train(d_model_A, d_model_B, g_model_AtoB, g_model_BtoA, c_model_AtoB, c_model_BtoA, dataset):
	# Define properties of the training run
    # For debugging set epoch to 1. For real training the authors suggested at least 100 epochs.
    # The authors write that they are using a batch size of 1. I wonder if a bigger batch size would not be better.
	number_of_epochs, number_of_batches, = 100, 1

	# Determine the output square shape of the discriminator
	n_patch = d_model_A.output_shape[1]

	# Unpack dataset
	trainA, trainB = dataset

	# Prepare image pool for fakes
	poolA, poolB = list(), list()

	# Calculate the number of batches per training epoch
	bat_per_epo = int(len(trainA) / number_of_batches)

	# Calculate the number of training iterations
    # This becomes big really fast, I should save the model in between.
	number_of_steps = bat_per_epo * number_of_epochs

	# Let the training begin
    # 1. Collect a batch of real images from each domain
    # 2. Generate a batch fake images for each domain
    # 3. Update the discriminators
    # 4. Update generator B->A and discriminatorA
    # 5. Update generator A->B and discriminatorB (4 and 5 can be switched)
    # 6. Display the performance
	for i in range(number_of_steps):
		# 1. Celect a batch of real samples
		X_realA, y_realA = generate_real_samples(trainA, number_of_batches, n_patch)
		X_realB, y_realB = generate_real_samples(trainB, number_of_batches, n_patch)
		# 2. Generate a batch of fake samples
		X_fakeA, y_fakeA = generate_fake_samples(g_model_BtoA, X_realB, n_patch)
		X_fakeB, y_fakeB = generate_fake_samples(g_model_AtoB, X_realA, n_patch)
		# 3. Update fake images from pool
		X_fakeA = update_image_pool(poolA, X_fakeA)
		X_fakeB = update_image_pool(poolB, X_fakeB)
		# 4. Update generator B->A via adversarial and cycle loss
		g_loss2, _, _, _, _  = c_model_BtoA.train_on_batch([X_realB, X_realA], [y_realA, X_realA, X_realB, X_realA])
		# 4. Update discriminator for A -> [real/fake]
		dA_loss1 = d_model_A.train_on_batch(X_realA, y_realA)
		dA_loss2 = d_model_A.train_on_batch(X_fakeA, y_fakeA)
		# 5. Update generator A->B via adversarial and cycle loss
		g_loss1, _, _, _, _ = c_model_AtoB.train_on_batch([X_realA, X_realB], [y_realB, X_realB, X_realA, X_realB])
		# 5. Update discriminator for B -> [real/fake]
		dB_loss1 = d_model_B.train_on_batch(X_realB, y_realB)
		dB_loss2 = d_model_B.train_on_batch(X_fakeB, y_fakeB)
		# 6. Summarize performance
		print('>%d, %d, dA[%.3f,%.3f] dB[%.3f,%.3f] g[%.3f,%.3f]' % (i+1, number_of_steps, dA_loss1,dA_loss2, dB_loss1,dB_loss2, g_loss1,g_loss2))
		
        # Evaluate the model performance in regular intervalls
		if (i+1) % (bat_per_epo * 1) == 0:
			# Plot A->B translation
			summarize_performance(i, g_model_AtoB, trainA, 'AtoB')
			# Plot B->A translation
			summarize_performance(i, g_model_BtoA, trainB, 'BtoA')
		if (i+1) % (bat_per_epo * 1) == 0:
			# Save the prelimanary model, since it takes so 
			# long to train the whole thing, this is important!
            # With the current setup it makes sense to save the model after each epoch (583 iterations).
			print('Saving prelimanary model')
			save_models(i, g_model_AtoB, g_model_BtoA)



################
# Execution code
################
# Load image data generated with dataPreparation.py
dataset = load_real_samples('cezanne2photo_256.npz')
print('Loaded', dataset[0].shape, dataset[1].shape)

# Define input shape based on the loaded dataset
image_shape = dataset[0].shape[1:]

###########################
# Initialize the generators
###########################
# Generaot: A -> B
generator_model_AtoB = define_generator(image_shape)
# Generator: B -> A
generator_model_BtoA = define_generator(image_shape)

# Discriminator: A -> [real/fake]
discriminator_model_A = define_discriminator(image_shape)
# Discriminator: B -> [real/fake]
discriminator_model_B = define_discriminator(image_shape)

# Define composite models for each of the generators
# Composite: A -> B -> [real/fake, A]
composite_model_AtoB = define_composite_model(generator_model_AtoB, discriminator_model_B, generator_model_BtoA, image_shape)
# Composite: B -> A -> [real/fake, B]
composite_model_BtoA = define_composite_model(generator_model_BtoA, discriminator_model_A, generator_model_AtoB, image_shape)

# Train models
train(discriminator_model_A, discriminator_model_B, generator_model_AtoB, generator_model_BtoA, composite_model_AtoB, composite_model_BtoA, dataset)