###################################################
# This code is used to load images and save them in 
# one consistent 32-bit floating point file
###################################################
from os import listdir
from numpy import asarray
from numpy import vstack
from keras.preprocessing.image import img_to_array
from keras.preprocessing.image import load_img
from numpy import savez_compressed
 
#Lload the images into memory
def load_images(path, size=(256,256)):
	data_list = list()
	# Enumerate filenames in directory. This assumes all files are images!
	for filename in listdir(path):
		# Load and resize the image if necessary
		pixels = load_img(path + filename, target_size=size)
		# Convert to numpy array
		pixels = img_to_array(pixels)
		# Append to our data list
		data_list.append(pixels)
	return asarray(data_list)
 
# Define the data paths. These files are taken from 
# "people.eecs.berkeley.edu/~taesung_park/CycleGAN/datasets"
path = 'cezanne2photo/'

# Load dataset A
dataA1 = load_images(path + 'trainA/')
dataAB = load_images(path + 'testA/')
dataA = vstack((dataA1, dataAB))
print('Loaded dataA: ', dataA.shape)

# Load dataset B
dataB1 = load_images(path + 'trainB/')
dataB2 = load_images(path + 'testB/')
dataB = vstack((dataB1, dataB2))
print('Loaded dataB: ', dataB.shape)

# Save as compressed numpy array
filename = 'cezanne2photo_256.npz'
savez_compressed(filename, dataA, dataB)
print('Saved dataset: ', filename)



# Test if it worked
# Load and plot the saved dataset
from numpy import load
from matplotlib import pyplot
# Load the dataset
data = load('cezanne2photo_256.npz')
dataA, dataB = data['arr_0'], data['arr_1']
print('Loaded: ', dataA.shape, dataB.shape)
# Plot domainA images
n_samples = 3
for i in range(n_samples):
	pyplot.subplot(2, n_samples, 1 + i)
	pyplot.axis('off')
	pyplot.imshow(dataA[i].astype('uint8'))
# Plot domainB images
for i in range(n_samples):
	pyplot.subplot(2, n_samples, 1 + n_samples + i)
	pyplot.axis('off')
	pyplot.imshow(dataB[i].astype('uint8'))
pyplot.show()